from __future__ import annotations

from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import roc_auc_score


import datetime
from typing import Optional, Union, Iterable, Sequence, Any, cast

import weakref


class InvalidClientError(ValueError):
    """Файл входных данных имеет недопустимое значение
    """


class Client:
    """Абстрактный суперкласс для всех клиентов

    Returns:
        _type_: _description_
    """

    def __init__(
        self,
        seniority: int,
        home: int,
        age: int,
        marital: int,
        records: int,
        expenses: int,
        assets: int,
        amount: int,
        price: int,
    ) -> None:
        self.seniority = seniority
        self.home = home
        self.age = age
        self.marital = marital
        self.records = records
        self.expenses = expenses
        self.assets = assets
        self.amount = amount
        self.price = price

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority}"
            f"home={self.home}"
            f"age={self.age}"
            f"marital={self.marital}"
            f"records={self.records}"
            f"expenses={self.assets}"
            f"amount={self.amount}"
            f"price={self.price}"
            f")"
        )


class KnownClient(Client):
    def __init__(
        self,
        status: int,
        seniority: int,
        home: int,
        age: int,
        marital: int,
        records: int,
        expenses: int,
        assets: int,
        amount: int,
        price: int,
    ) -> None:
        super().__init__(
            seniority=seniority,
            home=home,
            age=age,
            marital=marital,
            records=records,
            expenses=expenses,
            assets=assets,
            amount=amount,
            price=price,
        )
        self.status = status

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority},"
            f"home={self.home},"
            f"age={self.age},"
            f"marital={self.marital},"
            f"records={self.records},"
            f"expenses={self.assets},"
            f"amount={self.amount},"
            f"price={self.price},"
            f"status={self.status!r},"
            f")"
        )

    @classmethod
    def from_dict(cls, row: dict["str", "str"]) -> "KnownClient":
        if row["status"] not in {'1', '2', '0'}:
            raise InvalidClientError(f"Invalid client in {row!r}")
        try:
            return cls(
                seniority=int(row["seniority"]),
                home=int(row["home"]),
                age=int(row["age"]),
                marital=int(row["marital"]),
                records=int(row["records"]),
                expenses=int(row["expenses"]),
                assets=int(row["assets"]),
                amount=int(row["amount"]),
                price=int(row["price"]),
                status=int(row["status"]),
            )
        except ValueError as exeption:
            raise InvalidClientError(f"Invalid {row!r}")


class TrainingKnownClient(KnownClient):

    @classmethod
    def from_dict(cls, row: dict[str, str]) -> "TrainingKnownClient":
        return cast(TrainingKnownClient, super().from_dict(row))


class TestingKnownClient(KnownClient):
    def __init__(
        self,
        /,
        status: int,
        seniority: int,
        home: int,
        age: int,
        marital: int,
        records: int,
        expenses: int,
        assets: int,
        amount: int,
        price: int,
        classification: Optional[int] = None,
    ) -> None:
        super().__init__(
            status,
            seniority,
            home,
            age,
            marital,
            records,
            expenses,
            assets,
            amount,
            price,
        )
        self.classification = classification

    def mathces(self) -> bool:
        return self.status == self.classification

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority},"
            f"home={self.home},"
            f"age={self.age},"
            f"marital={self.marital},"
            f"records={self.records},"
            f"expenses={self.assets},"
            f"amount={self.amount},"
            f"price={self.price},"
            f"status={self.status!r},"
            f"classification={self.classification!r}"
            f")"
        )

    @classmethod
    def from_dict(cls, row: dict[str, str]) -> "TestingKnownClient":
        return cast(TestingKnownClient, super().from_dict(row))


class UnknownClient(Client):

    @classmethod
    def from_dict(cls, row: dict[str, str]) -> "UnknownClient":
        if set(row.keys()) != {
            "seniority",
            "home",
            "age",
            "marital",
            "records",
            "assets",
            "amount",
            "price",
        }:
            raise InvalidClientError(f"Invalid fields in {row!r}")
        try:
            return cls(
                seniority=int(row["seniority"]),
                home=int(row["home"]),
                age=int(row["age"]),
                marital=int(row["marital"]),
                records=int(row["records"]),
                expenses=int(row["expenses"]),
                assets=int(row["assets"]),
                amount=int(row["amount"]),
                price=int(row["price"]),
            )
        except (KeyError, ValueError):
            raise InvalidClientError(f"invalid {row!r}")


class ClassifiedClient(Client):
    def __init__(self, classification: Optional[int], client: UnknownClient) -> None:
        super().__init__(
            seniority=client.seniority,
            home=client.home,
            age=client.age,
            marital=client.marital,
            records=client.records,
            expenses=client.expenses,
            assets=client.assets,
            amount=client.amount,
            price=client.price,
        )
        self.classification = classification

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}("
            f"seniority={self.seniority},"
            f"home={self.home},"
            f"age={self.age},"
            f"marital={self.marital},"
            f"records={self.records},"
            f"expenses={self.assets},"
            f"amount={self.amount},"
            f"price={self.price},"
            f"classification={self.classification!r}"
            f")"
        )


class Hyperparameter:
    def __init__(
        self, max_depth: int, min_sample_size: int, training: "TrainingData"
    ) -> None:
        self.max_depth = max_depth
        self.min_leaf_size = min_sample_size
        self.data: weakref.ReferenceType["TrainingData"] = weakref.ref(training)
        self.quality: float

    def test(self) -> None:
        training_data: Optional["TrainingData"] = self.data()
        if not training_data:
            raise RuntimeError("Broken Waek Reference")
        test_data = training_data.testing
        y_test = TrainingData.get_statuses_clients(test_data)
        y_predict = self.classify_list(test_data)
        self.quality = roc_auc_score(y_test, y_predict)
        for i in range(len(y_predict)):
            test_data[i].classification = y_predict[i]

    def classify_list(
        self, clients: Sequence[Union[UnknownClient, TestingKnownClient]]
    ) -> list[Any]:
        training_data = self.data()
        if not training_data:
            raise RuntimeError("No training object")
        x_predict = TrainingData.get_list_clients(clients)
        x_train = TrainingData.get_list_clients(training_data.training)
        y_train = TrainingData.get_statuses_clients(training_data.training)

        classifier = DecisionTreeClassifier()
        classifier = classifier.fit(x_train, y_train)
        y_pred = classifier.predict(x_predict).tolist()
        return [y_pred]


class TrainingData:
    def __init__(self, name: str) -> None:
        self.name = name
        self.uploaded: datetime.datetime
        self.tested: datetime.datetime
        self.training: list[TrainingKnownClient] = []
        self.testing: list[TestingKnownClient] = []
        self.tuning: list[Hyperparameter] = []

    def load(self, raw_data_soruce: Iterable[dict[str, str]]) -> None:
        """
        Загружает и разбивается исходные данные

        Args:
            raw_data_soruce (Iterable[dict[str, str]]): Источник данных
        """

        for n, row in enumerate(raw_data_soruce):
            if n % 5 == 0:
                testing_client = TestingKnownClient.from_dict(row)
                self.testing.append(testing_client)
            else:
                training_data = TrainingKnownClient.from_dict(row)
                self.training.append(training_data)
        self.uploaded = datetime.datetime.now(tz=datetime.timezone.utc)

    def test(self, parameter: Hyperparameter) -> None:
        """

        Args:
            parameter (Hyperparameter): Гиперпараметры
        """

        parameter.test()
        self.tuning.append(parameter)
        self.tested = datetime.datetime.now(tz=datetime.timezone.utc)

    # Похоже на статический метод вообще)
    def classify(
        self, parameter: Hyperparameter, client: UnknownClient
    ) -> ClassifiedClient:
        """

        Args:
            parameter (Hyperparameter): _description_
            client (Client): _description_

        Returns:
            Client: _description_
        """

        return ClassifiedClient(
            classification=parameter.classify_list([client])[0], client=client
        )

    @staticmethod
    def get_list_clients(clients: Sequence[Client]) -> list[list[int]]:
        return [
            [
                client.seniority,
                client.home,
                client.age,
                client.marital,
                client.records,
                client.expenses,
                client.assets,
                client.amount,
                client.price,
            ]
            for client in clients
        ]

    @staticmethod
    def get_statuses_clients(clients: Sequence[KnownClient]) -> list[int]:
        return [client.status for client in clients]

    # Получить список свойств клиента для классификации
    @staticmethod
    def get_client_as_list(client: Client) -> list[list[int]]:
        return [
            [
                client.seniority,
                client.home,
                client.age,
                client.marital,
                client.records,
                client.expenses,
                client.assets,
                client.amount,
                client.price,
            ]
        ]
