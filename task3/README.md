Порядок запуска:

- Переходим в терминале в папку task 3
```shell
cd task3
```

- Создаем виртуальное окружение интерпретатора питона:
```shell
python -m venv env
```
-  Цепляем виртуальное окружение как источник

На винде
```shell
env\Scripts\activate.bat
```
На unix-like
```shell
source env/bin/activate
```
- Устанавливаем зависимости
```shell
pip install -r requirements.txt
```
- Теперь открываем командную строку интерпертатора
```shell
python
```
- В оболочке python импортируем нужный модуль
```python
from src.models import *
```

- Дальше уже можем играться с классами аналогично его примеру, только вставляя свое